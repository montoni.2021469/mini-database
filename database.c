#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "database.h"

IndexNodeString* create_tree_String(char* root_value, Persona* persona);
IndexNodeInt* create_tree_int(int root_value, Persona* persona);
void insert_String(IndexNodeString* root, char* value,Persona* persona);
void insert_int(IndexNodeInt* root, int value,Persona* persona);
Persona* find_string(IndexNodeString* root, char* s);
Persona* find_int(IndexNodeInt* root, int i);

//Auxiliary functions
IndexNodeString* create_tree_String(char* root_value,Persona* persona)  {
    IndexNodeString* root = (IndexNodeString*) malloc(sizeof(IndexNodeString));
    if (root == NULL) {
        return NULL;
    } 
    root->value = root_value;
    root->left = NULL;
    root->right = NULL;
    root->persona=persona;
    return root;
}

IndexNodeInt* create_tree_int(int root_value, Persona* persona) {
    IndexNodeInt* root = (IndexNodeInt*) malloc(sizeof(IndexNodeInt));
    if (root == NULL) {
        return NULL;
    } 
    root->value = root_value;
    root->left = NULL;
    root->right = NULL;
    root->persona=persona;
    return root;
}

//Database creation
Database* create_Database(Persona* persona) {
    Database* root=(Database*) malloc(sizeof(Database));
    if (root == NULL) {
        return NULL;
    } 
    root->name = create_tree_String(persona->name,persona);
    root->surname=create_tree_String(persona->surname,persona);
    root->address=create_tree_String(persona->address,persona);
    root->age=create_tree_int(persona->age,persona);
    return root;
}

//Auxiliary functions
void insert_String(IndexNodeString* root, char* value, Persona* persona) {
    if (root == NULL) {
        return;
    }
    if (strcmp(value,root->value)<=0) {
        if (root->left == NULL) {
            IndexNodeString* node = create_tree_String(value,persona);
            root->left = node;
            return;
        }
        insert_String(root->left, value,persona);
        return;
    }
    if (root->right == NULL) {
        IndexNodeString* node = create_tree_String(value,persona);
        root->right = node;
        return;
    }
    insert_String(root->right, value,persona);
}

void insert_int(IndexNodeInt* root, int value, Persona* persona) {
    if (root == NULL) {
        return;
    }
    if (value <= root->value) {
        if (root->left == NULL) {
            IndexNodeInt* node = create_tree_int(value,persona);
            root->left = node;
            return;
        }
        insert_int(root->left, value, persona);
        return;
    }
    if (root->right == NULL) {
        IndexNodeInt* node = create_tree_int(value, persona);
        root->right = node;
        return;
    }
    insert_int(root->right, value, persona);
}

//Database insertion
void insert(Database * database, Persona * persona) {
    if (database == NULL) {
        return;
    }
    
    insert_String(database->name, persona->name, persona);
    insert_String(database->surname, persona->surname, persona);
    insert_String(database->address, persona->address, persona);
    insert_int(database->age,persona->age, persona);
    
    return;
}

//Auxiliary functions
Persona* find_string(IndexNodeString* root, char* s) {
    if(root==NULL) return NULL;
    int compare=strcmp(root->value,s);
    if(compare==0) return root->persona;
    return (compare>0)? find_string(root->left,s) : find_string(root->right,s);
}

Persona* find_int(IndexNodeInt* root, int i) {
    if(root==NULL) return NULL;
    if(root->value==i) return root->persona;
    return (root->value>i)? find_int(root->left,i) : find_int(root->right,i);
}

//Find Person by name
Persona* findByName(Database * database, char * name) {
    if(database==NULL) return NULL;
    return find_string(database->name,name);
}

//Find Person by surname
Persona* findBySurname(Database * database, char * surname) {
    if(database==NULL) return NULL;
    return find_string(database->surname,surname);
}

//Find Person by address
Persona* findByAddress(Database * database, char * address) {
    if(database==NULL) return NULL;
    return find_string(database->address,address);
}

//Find Person by age
Persona* findByAge(Database * database, int age) {
    if(database==NULL) return NULL;
    return find_int(database->age,age);
}

//Auxiliary functions
void free_tree_string(IndexNodeString* root) {
    if(root==NULL) return;
    free_tree_string(root->left);
    free_tree_string(root->right);
    free(root);
}

void free_tree_int(IndexNodeInt* root) {
    if(root==NULL) return;
    free_tree_int(root->left);
    free_tree_int(root->right);
    free(root);
}

//Free database
void free_Database(Database* database) {
    free_tree_string(database->name);
    free_tree_string(database->surname);
    free_tree_string(database->address);
    free_tree_int(database->age);
    free(database);
}

