#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "database.h"

Persona* create_Persona(char* name, char* surname, char* address, int age);
void print_Database(Database* database); 

int main(int argc, char** argv) {
    Persona* primo=create_Persona("Mario","Rossi","Via Blu 18",32);
    Database* data1=create_Database(primo);
    Persona* secondo=create_Persona("Luigi","Blu","Via Rossi 81",23);
    Persona* terzo=create_Persona("Francesco","Papi","Via Verdi 10",67);
    Persona* quarto=create_Persona("Francesca","Zola","Via Giove 50",10);
    
    insert(data1,secondo);
    insert(data1,terzo);
    insert(data1,quarto);
    
    printf("Primo Database\n");
    print_Database(data1);

    Database* data2=create_Database(findByName(data1,"Luigi"));
    insert(data2, findBySurname(data1,"Papi"));
    insert(data2,findByAddress(data1,"Via Blu 18"));
    insert(data2, findByAge(data1, 10));

    printf("\n\n\nSecondo Database\n");
    print_Database(data2);

    free(primo);
    free(secondo);
    free(terzo);
    free(quarto);
    free_Database(data1);
    free_Database(data2);
    return 0;
}

//Persona creation
Persona* create_Persona(char* name, char* surname, char* address, int age) {
    Persona* persona=(Persona*)malloc(sizeof(Persona));
    memcpy(persona->name,name,20);
    memcpy(persona->surname,surname,50);
    memcpy(persona->address,address,100);
    persona->age=age;

    return persona;
}

//Print Database
void print_Node_String(IndexNodeString* root) {
    if (root == NULL) {
        return;
    }
    print_Node_String(root->left);
    printf("%s\n", root->value);
    print_Node_String(root->right);
}

void print_Node_int(IndexNodeInt* root) {
    if (root == NULL) {
        return;
    }
    print_Node_int(root->left);
    printf("%d\n", root->value);
    print_Node_int(root->right);
}

void print_Database(Database* database) {
    if (database == NULL) {
        return;
    }
    print_Node_String(database->name);
    print_Node_String(database->surname);
    print_Node_String(database->address);
    print_Node_int(database->age);

}